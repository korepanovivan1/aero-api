<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

/**
 * @extends Factory<User>
 */
class UserFactory extends Factory
{

    private const USER_PASSWORD = 'password';

    private const PHONE_NUMBER_MASK = '8##########';
    public const DOCUMENT_NUMBER_MASK = '##########';

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'first_name' => fake()->firstName(),
            'last_name' => fake()->lastName(),
            'phone' => fake()->unique()->numerify(self::PHONE_NUMBER_MASK),
            'password' => Hash::make(self::USER_PASSWORD),
            'document_number' => fake()->numerify(self::DOCUMENT_NUMBER_MASK),
            'remember_token' => Str::random(10),
        ];
    }
}
