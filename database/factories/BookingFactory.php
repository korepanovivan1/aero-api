<?php

namespace Database\Factories;

use App\Models\Booking;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends Factory<Booking>
 */
class BookingFactory extends Factory
{
    private const CODE_MASK = '?????';

    private const DATE_FROM_INTERVAL = '+2 months';
    public const DATE_TO_INTERVAL_AFTER_DATE_FROM = '+1 months';

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'code' => Str::upper(fake()->unique()->lexify(self::CODE_MASK)),
            'date_from' => fake()->dateTimeInInterval('now', self::DATE_FROM_INTERVAL)
        ];
    }
}
