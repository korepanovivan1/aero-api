<?php

namespace Database\Factories;

use App\Models\Passenger;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<Passenger>
 */
class PassengerFactory extends Factory
{
    private const PASSENGER_MIN_AGE = 18;
    private const PASSENGER_MAX_AGE = 60;

    private const PASSENGER_BIRTH_DATE_INTERVAL = self::PASSENGER_MAX_AGE - self::PASSENGER_MIN_AGE;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'first_name' => fake()->firstName(),
            'last_name' => fake()->lastName(),
            'birth_date' => fake()->dateTimeInInterval(
                '-' . self::PASSENGER_MAX_AGE . ' years',
                '+' . self::PASSENGER_BIRTH_DATE_INTERVAL . ' years',
            ),
            'document_number' => fake()->numerify(UserFactory::DOCUMENT_NUMBER_MASK),
        ];
    }
}
