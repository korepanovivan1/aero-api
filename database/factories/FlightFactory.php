<?php

namespace Database\Factories;

use App\Models\Flight;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<Flight>
 */
class FlightFactory extends Factory
{
    private const CODE_MASK = '??-#####';
    private const TIME_FORMAT = 'H:i';

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $from = fake()->time(self::TIME_FORMAT);
        $to = $this->faker->dateTimeInInterval($from, '+2 hours')->format(self::TIME_FORMAT);

        return [
            'code' => strtoupper(fake()->unique()->bothify(self::CODE_MASK)),
            'time_from' => $from,
            'time_to' => $to,
            'cost' => round(fake()->randomNumber(5), -2)
        ];
    }
}
