<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TruncateTablesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('users')->truncate();
        DB::table('bookings')->truncate();
        DB::table('passengers')->truncate();
        DB::table('flights')->truncate();
        DB::table('airports')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
