<?php

namespace Database\Seeders;

use App\Models\Airport;
use App\Models\Booking;
use App\Models\Flight;
use App\Models\Passenger;
use App\Models\User;
use Database\Factories\BookingFactory;
use Exception;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;

class AirlineSeeder extends Seeder
{
    private const FLIGHT_KEY_MASK = '%s_%s';

    private const PLACES_ROWS_LETTERS = ['A', 'B', 'C', 'D'];
    private const COUNT_PLACES_IN_ROW = 12;

    private const MIN_FLIGHTS_FOR_AIRPORTS = 1;
    private const MAX_FLIGHTS_FOR_AIRPORTS = 10;
    private const MAX_BOOKINGS_FOR_FLIGHT = 3;
    private const MAX_PASSENGERS_FOR_BOOKING = 8;

    private array $availableFlightPlaces = [];

    /**
     * Run the database seeds.
     *
     * @return void
     * @throws Exception
     */
    public function run(): void
    {
        $usersDocumentsNumbers = $this->createUsers(5);

        $airportIds = $this->createAirports();
        $flights = $this->createFlights($airportIds);
        $this->createBookingsWithPassengers($flights, $usersDocumentsNumbers);
    }

    private function createUsers(int $countUsers = 1): array
    {
        $users = [];

        for ($i = 0; $i < $countUsers; $i++) {
            $users[] = User::factory()->create();
        }

        return $users;
    }

    private function createAirports(): array
    {
        $airports = $this->getAirportsList();
        $airportIds = [];

        foreach ($airports as $airport) {
            $airportIds[] = Airport::factory()->state($airport)->create()->id;
        }

        return $airportIds;
    }

    private function createFlights(array $airportsIds): array
    {
        $flights = [];

        foreach ($this->getDifferentPairsWithReversePairs($airportsIds) as $pair) {
            $flightCounter = random_int(self::MIN_FLIGHTS_FOR_AIRPORTS, self::MAX_FLIGHTS_FOR_AIRPORTS);

            for ($i = 0; $i < $flightCounter; $i++) {
                $flight = Flight::factory()->state([
                    'airport_from_id' => $pair[0],
                    'airport_to_id' => $pair[1],
                ])->create();

                $flightKey = sprintf(
                    self::FLIGHT_KEY_MASK,
                    $flight->airport_from_id,
                    $flight->airport_to_id
                );

                $this->availableFlightPlaces[$flight->id] = $this->generatePlacesList();

                $flights[$flightKey] = $flight;
            }
        }

        return $flights;
    }

    /**
     * @throws Exception
     */
    private function createBookingsWithPassengers(
        array $flights,
        array $usersDocumentsNumbers
    ): void {
        foreach ($flights as $flightFrom) {
            $countBookings = random_int(1, self::MAX_BOOKINGS_FOR_FLIGHT);

            for ($i = 0; $i < $countBookings; $i++) {
                $booking = $this->createBookingByFlights($flights, $flightFrom);

                $this->createPassengers($booking, $usersDocumentsNumbers);
            }
        }
    }

    private function createBookingByFlights(array $flights, Flight $flightFrom): ?Booking
    {
        $hasFlightBack = fake()->boolean();

        $booking = Booking::factory()->state([
            'flight_from_id' => $flightFrom->id
        ])->make();

        if ($hasFlightBack && isset($flightFrom->airport_to_id) && isset($flightFrom->airport_from_id)) {
            //если есть обратный рейс, то ищем рейс с аэропортами прямого, но меняем их местами
            $flightBackKey = sprintf(
                self::FLIGHT_KEY_MASK,
                $flightFrom->airport_to_id,
                $flightFrom->airport_from_id
            );

            if(!isset($flights[$flightBackKey])) {
                return null;
            }

            $flightBack = $flights[$flightBackKey];

            $booking->flight_back_id = $flightBack->id;
            $booking->date_back = fake()->dateTimeInInterval(
                $booking->date_from,
                BookingFactory::DATE_TO_INTERVAL_AFTER_DATE_FROM
            );
        }

        $booking->save();

        return $booking;
    }

    /**
     * @throws Exception
     */
    private function createPassengers(
        Booking $booking,
        array $usersDocumentsNumbers
    ): void {
        $countBookingPassengers = random_int(1, self::MAX_PASSENGERS_FOR_BOOKING);
        //привязывать ли бронирование к пользователю по номеру документа одного из пассажиров
        $hasBindBookingToUser = fake()->boolean();

        for ($i = 0; $i < $countBookingPassengers; $i++) {
            $placeFrom = fake()->boolean() ? array_shift(
                $this->availableFlightPlaces[$booking->flight_from_id]
            ) : null;

            $state = [
                'booking_id' => $booking->id,
                'place_from' => $placeFrom
            ];

            if (isset($booking->flight_back_id) && fake()->boolean()) {
                $placeBack = array_shift(
                    $this->availableFlightPlaces[$booking->flight_back_id]
                );

                $state['place_back'] = $placeBack;
            }

            if ($i === 0 && $hasBindBookingToUser) {
                $bindedUser = fake()->randomElement($usersDocumentsNumbers);

                $state['document_number'] = $bindedUser->document_number;
                $state['first_name'] = $bindedUser->first_name;
                $state['last_name'] = $bindedUser->last_name;
            }

            Passenger::factory()->state($state)->create();
        }
    }

    private function getAirportsList(): array
    {
        return [
            ['iata' => 'VKO', 'name' => 'Внуково', 'city' => 'Москва'],
            ['iata' => 'DME', 'name' => 'Домодедово', 'city' => 'Москва'],
            ['iata' => 'KGD', 'name' => 'Храброво', 'city' => 'Калининград'],
            ['iata' => 'IJK', 'name' => 'Ижевск', 'city' => 'Ижевск'],
            ['iata' => 'AER', 'name' => 'Адлер', 'city' => 'Сочи'],
            ['iata' => 'OVB', 'name' => 'Толмачёво', 'city' => 'Новосибирск'],
            ['iata' => 'TOF', 'name' => 'Богашёво', 'city' => 'Томск'],
            ['iata' => 'KRR', 'name' => 'Пашковский', 'city' => 'Краснодар'],
            ['iata' => 'ESL', 'name' => 'Элиста', 'city' => 'Элиста'],
            ['iata' => 'HMA', 'name' => 'Ханты-Мансийск', 'city' => 'Ханты-Мансийск'],
            ['iata' => 'NNM', 'name' => 'Нарьян-Мар', 'city' => 'Нарьян-Мар'],
            ['iata' => 'LED', 'name' => 'Пулково', 'city' => 'Санкт-Петербург'],
            ['iata' => 'BZK', 'name' => 'Брянск', 'city' => 'Брянск'],
            ['iata' => 'MQF', 'name' => 'Магнитогорск', 'city' => 'Магнитогорск'],
            ['iata' => 'ROV', 'name' => 'Платов', 'city' => 'Ростов-на-Дону'],
        ];
    }

    private function generatePlacesList(): array
    {
        $result = [];

        foreach (self::PLACES_ROWS_LETTERS as $rowLetter) {
            for ($i = 1; $i <= self::COUNT_PLACES_IN_ROW; $i++) {
                $result[] = $rowLetter . $i;
            }
        }

        return Arr::shuffle($result);
    }

    private function getDifferentPairsWithReversePairs($arr): array
    {
        $pairs = [];
        $n = count($arr);

        foreach ($arr as $i => $iValue) {
            for ($j = $i + 1; $j < $n; $j++) {
                if ($iValue !== $arr[$j]) {
                    $pairs[] = [$iValue, $arr[$j]];
                    $pairs[] = [$arr[$j], $iValue];
                }
            }
        }

        return $pairs;
    }
}
