<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * App\Models\Booking
 *
 * @property int $id
 * @property string $code
 * @property int $flight_from_id
 * @property int|null $flight_back_id
 * @property string $date_from
 * @property string|null $date_back
 * @property-read \App\Models\Flight|null $flightBack
 * @property-read \App\Models\Flight|null $flightFrom
 * @property-read \Illuminate\Database\Eloquent\Collection<int, \App\Models\Passenger> $passengers
 * @property-read int|null $passengers_count
 * @method static \Illuminate\Database\Eloquent\Builder|Booking newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Booking newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Booking query()
 * @method static \Illuminate\Database\Eloquent\Builder|Booking whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Booking whereDateBack($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Booking whereDateFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Booking whereFlightBackId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Booking whereFlightFromId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Booking whereId($value)
 * @mixin \Eloquent
 */
class Booking extends Model
{
    use HasFactory;

    public const MAX_COUNT_PASSENGER = 48;

    public $timestamps = false;

    protected $fillable = [
        'code',
        'flight_from_id',
        'flight_back_id',
        'date_from',
        'date_back',
    ];

    public function passengers(): HasMany
    {
        return $this->hasMany(Passenger::class);
    }

    public function flightFrom(): HasOne
    {
        return $this->hasOne(Flight::class, 'id', 'flight_from_id');
    }

    public function flightBack(): HasOne
    {
        return $this->hasOne(Flight::class, 'id', 'flight_back_id');
    }
}
