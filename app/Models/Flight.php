<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * App\Models\Flight
 *
 * @property int $id
 * @property string $code
 * @property int $airport_from_id
 * @property int $airport_to_id
 * @property string $time_from
 * @property string $time_to
 * @property int $cost
 * @property-read \App\Models\Airport $arrivalAirport
 * @property-read \App\Models\Airport $departureAirport
 * @method static \Illuminate\Database\Eloquent\Builder|Flight newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Flight newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Flight query()
 * @method static \Illuminate\Database\Eloquent\Builder|Flight whereAirportFromId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Flight whereAirportToId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Flight whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Flight whereCost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Flight whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Flight whereTimeFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Flight whereTimeTo($value)
 * @mixin \Eloquent
 */
class Flight extends Model
{
    use HasFactory;

    public $timestamps = false;

    public function departureAirport(): BelongsTo
    {
        return $this->belongsTo(Airport::class, 'airport_from_id');
    }

    public function arrivalAirport(): BelongsTo
    {
        return $this->belongsTo(Airport::class, 'airport_to_id');
    }
}
