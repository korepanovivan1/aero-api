<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Passenger
 *
 * @property int $id
 * @property int $booking_id
 * @property string $first_name
 * @property string $last_name
 * @property string $birth_date
 * @property string $document_number
 * @property string|null $place_from
 * @property string|null $place_back
 * @property string|null $created_at
 * @property string|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Passenger newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Passenger newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Passenger query()
 * @method static \Illuminate\Database\Eloquent\Builder|Passenger whereBirthDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Passenger whereBookingId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Passenger whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Passenger whereDocumentNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Passenger whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Passenger whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Passenger whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Passenger wherePlaceBack($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Passenger wherePlaceFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Passenger whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Passenger extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [
        'booking_id',
        'first_name',
        'last_name',
        'birth_date',
        'document_number',
        'place_from',
        'place_back',
    ];
}
