<?php

namespace App\Interfaces;

use App\Models\Booking;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

interface BookingRepositoryInterface
{
    public function create(array $data): Booking;

    public function existsBookingByCode(string $code): bool;

    public function countPassengerToBooking(string $type, string $date, int $flightId): int;

    public function getBookingByUser(string $docNumber): LengthAwarePaginator;

    public function checkFlightBackAvailability(int $bookingId): bool;
}