<?php

namespace App\Interfaces;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

interface FlightRepositoryInterface
{
    public function getFlightsByAirports(
        string $departureAirportIata,
        string $arrivalAirportIata
    ): LengthAwarePaginator;

    public function getOccupiedSeats(int $flightId, string $flightDate): Collection;
}
