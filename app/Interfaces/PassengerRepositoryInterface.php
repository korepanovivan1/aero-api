<?php

namespace App\Interfaces;

use App\Models\Booking;
use App\Models\Passenger;

interface PassengerRepositoryInterface
{
    public function create(array $data): Passenger;

    public function uniquePassenger(string $docNumber, int $flightFromId, ?int $flightBackId): bool;

    public function update(Booking $booking, int $passengerId, array $data): Passenger;
}