<?php

namespace App\Interfaces;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;

interface AirportRepositoryInterface
{
    public function getAirportsByFilterQuery(): LengthAwarePaginator;
}
