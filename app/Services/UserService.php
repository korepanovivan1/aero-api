<?php

namespace App\Services;

use App\Interfaces\BookingRepositoryInterface;
use App\Models\User;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

class UserService
{
    public function __construct(
        private BookingRepositoryInterface $bookingRepository
    ) {}

    public function getBooking(User $user): LengthAwarePaginator
    {
        return $this->bookingRepository->getBookingByUser($user->document_number);
    }
}