<?php

namespace App\Services;

use App\Interfaces\FlightRepositoryInterface;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

class FlightService
{
    private const PLACES_ROWS_LETTERS = ['A', 'B', 'C', 'D'];
    private const COUNT_PLACES_IN_ROW = 12;

    public function __construct(private FlightRepositoryInterface $flightRepository)
    {
    }

    public function getFlightsByDate(
        string $departureAirportIata,
        string $arrivalAirportIata,
        string $flightDate
    ): LengthAwarePaginator {
        $flights = $this->flightRepository->getFlightsByAirports($departureAirportIata, $arrivalAirportIata);

        foreach ($flights as $flight) {
            $flight->date = $flightDate;
        }

        return $flights;
    }

    public function getSeats(int $flightId, string $flightDate): array
    {
        $seatsList = $this->generateSeatsList();
        $occupiedSeats = $this->flightRepository->getOccupiedSeats($flightId, $flightDate)->toArray();

        $result = [];

        foreach ($seatsList as $value) {
            $firstLetter = substr($value, 0, 1);
            $secondNumber = substr($value, 1);

            if (!isset($result[$firstLetter])) {
                $result[$firstLetter] = [];
            }

            $result[$firstLetter][$secondNumber] = in_array($value, $occupiedSeats);
        }

        return $result;
    }

    private function generateSeatsList(): array
    {
        $result = [];

        foreach (self::PLACES_ROWS_LETTERS as $rowLetter) {
            for ($i = 1; $i <= self::COUNT_PLACES_IN_ROW; $i++) {
                $result[] = $rowLetter.$i;
            }
        }

        return $result;
    }
}
