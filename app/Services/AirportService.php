<?php

namespace App\Services;

use App\Interfaces\AirportRepositoryInterface;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

class AirportService
{
    public function __construct(
        private AirportRepositoryInterface $airportRepository
    ) {}

    public function getAirports(): LengthAwarePaginator
    {
        return $this->airportRepository->getAirportsByFilterQuery();
    }
}
