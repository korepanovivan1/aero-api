<?php

namespace App\Services;

use App\Interfaces\PassengerRepositoryInterface;
use App\Models\Booking;
use App\Models\Passenger;

class PassengerService
{
    public function __construct(
        private PassengerRepositoryInterface $passengerRepository
    ) {}

    public function create(array $data): Passenger
    {
        return $this->passengerRepository->create($data);
    }

    public function update(Booking $booking, int $passengerId, array $data): Passenger
    {
        return $this->passengerRepository->update($booking, $passengerId, $data);
    }
}
