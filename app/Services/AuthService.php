<?php

namespace App\Services;

use App\Interfaces\UserRepositoryInterface;
use Illuminate\Support\Facades\Hash;

class AuthService
{
    public function __construct(
        private UserRepositoryInterface $userRepository
    ) {}

    public function register(array $data): bool
    {
        $data['password'] = Hash::make($data['password']);
        return $this->userRepository->create($data);
    }
}