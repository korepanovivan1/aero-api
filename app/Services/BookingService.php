<?php

namespace App\Services;

use App\Interfaces\BookingRepositoryInterface;
use App\Models\Booking;
use App\Models\Passenger;
use Illuminate\Support\Str;

class BookingService
{
    public function __construct(
        private BookingRepositoryInterface $bookingRepository,
        private PassengerService $passengerService,
    ) {}

    public function booking(array $data): Booking
    {
        $data = collect($data);
        $flightFrom = $data->get('flight_from');

        $dataBooking = [
            'flight_from_id' => $flightFrom['id'],
            'date_from' => $flightFrom['date'],
            'code' => $this->generateBookingCode(),
        ];

        if ($flightBack = $data->get('flight_back')) {
            $dataBooking = array_merge($dataBooking, [
                'flight_back_id' => $flightBack['id'],
                'date_back' => $flightBack['date'],
            ]);
        }

        $booking = $this->bookingRepository->create($dataBooking);

        foreach ($data->get('passengers', []) as $passenger) {
            $passenger['booking_id'] = $booking->id;
            $this->passengerService->create($passenger);
        }

        return $booking;
    }

    public function bookingSeat(Booking $booking, array $data): Passenger
    {
        $columnName = $data['type'] === 'from' ? 'place_from' : 'place_back';

        return $this->passengerService->update($booking, $data['passenger'], [
            $columnName => $data['seat'],
        ]);
    }

    private function generateBookingCode(): string
    {
        $code = Str::upper(Str::random(5));

        if ($this->bookingRepository->existsBookingByCode($code)) {
            $this->generateBookingCode();
        }

        return $code;
    }
}
