<?php

namespace App\Repositories;

use App\Interfaces\PassengerRepositoryInterface;
use App\Models\Booking;
use App\Models\Passenger;
use Illuminate\Database\Query\JoinClause;

class PassengerRepository implements PassengerRepositoryInterface
{
    public function create(array $data): Passenger
    {
        return Passenger::query()->create($data);
    }


    public function uniquePassenger(string $docNumber, int $flightFromId, ?int $flightBackId): bool
    {
        return Passenger::query()
            ->where('document_number', $docNumber)
            ->join('bookings', function (JoinClause $join) use ($docNumber, $flightFromId, $flightBackId) {
                $join->on('passengers.booking_id', '=', 'bookings.id')
                    ->where('flight_from_id', $flightFromId)
                    ->where('flight_back_id', $flightBackId);
            })
            ->exists();
    }

    public function update(Booking $booking, int $passengerId, array $data): Passenger
    {
        $passenger = Passenger::query()
            ->where('booking_id', $booking->id)
            ->where('id', $passengerId);
        $passenger->update($data);

        return $passenger->first();
    }
}