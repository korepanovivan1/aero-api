<?php

namespace App\Repositories;

use App\Interfaces\FlightRepositoryInterface;
use App\Models\Flight;
use App\Models\Passenger;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

class FlightRepository implements FlightRepositoryInterface
{
    const PER_PAGE = 5;

    public function getFlightsByAirports(string $departureAirportIata, string $arrivalAirportIata): LengthAwarePaginator
    {
        return Flight::with('departureAirport', 'arrivalAirport')->whereHas(
            'departureAirport', function ($query) use ($departureAirportIata) {
            $query->where('iata', $departureAirportIata);
        }
        )->whereHas('arrivalAirport', function ($query) use ($arrivalAirportIata) {
            $query->where('iata', $arrivalAirportIata);
        })->paginate(self::PER_PAGE);
    }

    public function getOccupiedSeats(int $flightId, string $flightDate): Collection
    {
        return Passenger::query()->selectRaw(
            'CASE WHEN flight_from_id = ? THEN place_from
                           WHEN flight_back_id = ? THEN place_back
                           ELSE NULL
                      END AS place', [$flightId, $flightId]
        )->join('bookings', 'passengers.booking_id', '=', 'bookings.id')->where([
            ['flight_from_id', $flightId],
            ['date_from', $flightDate]
        ])->orWhere([
            ['flight_back_id', $flightId],
            ['date_back', $flightDate]
        ])->havingNotNull('place')->pluck('place');
    }
}
