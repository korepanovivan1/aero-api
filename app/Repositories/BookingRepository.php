<?php

namespace App\Repositories;

use App\Interfaces\BookingRepositoryInterface;
use App\Models\Booking;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

class BookingRepository implements BookingRepositoryInterface
{
    private const PER_PAGE = 5;

    public function create(array $data): Booking
    {
        return Booking::query()->create($data);
    }

    public function existsBookingByCode(string $code): bool
    {
        return Booking::query()->where('code', $code)->exists();
    }

    public function countPassengerToBooking(string $type, string $date, int $flightId): int
    {
        if ($type === 'from') {
            $columnId = 'flight_from_id';
            $columnDate = 'date_from';
        } else {
            $columnId = 'flight_back_id';
            $columnDate = 'date_back';
        }

        return Booking::query()->where($columnId, $flightId)
            ->where($columnDate, $date)
            ->join('passengers', 'booking_id', '=', 'bookings.id')
            ->count();
    }

    public function getBookingByUser(string $docNumber): LengthAwarePaginator
    {
        return Booking::query()->with(['flightFrom', 'flightBack'])
            ->whereHas('passengers', function ($query) use ($docNumber) {
                $query->where('document_number', $docNumber);
            })->paginate(self::PER_PAGE);
    }

    public function checkFlightBackAvailability(int $bookingId): bool
    {
        return Booking::query()
            ->where('id', $bookingId)
            ->whereNotNull('flight_back_id')
            ->exists();
    }
}