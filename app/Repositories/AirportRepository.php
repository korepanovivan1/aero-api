<?php

namespace App\Repositories;

use App\Interfaces\AirportRepositoryInterface;
use App\Models\Airport;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Spatie\QueryBuilder\QueryBuilder;

class AirportRepository implements AirportRepositoryInterface
{
    private const PER_PAGE = 10;

    public function getAirportsByFilterQuery(): LengthAwarePaginator
    {
        return QueryBuilder::for(Airport::class)
            ->allowedFilters(['iata', 'name', 'city'])
            ->allowedSorts('name')
            ->defaultSorts('name')
            ->paginate(self::PER_PAGE)
            ->appends([request()?->get('filter'), request()?->get('sort')]);
    }
}
