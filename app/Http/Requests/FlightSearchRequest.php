<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FlightSearchRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return [
            'departureAirport' => 'required|exists:airports,iata',
            'arrivalAirport' => 'required|exists:airports,iata',
            'date' => 'required|date_format:Y-m-d|after:today',
            'passengers' => 'required|integer|between:1,48',
        ];
    }

    public function attributes(): array
    {
        return [
            'departureAirport' => 'Аэропорт вылета',
            'arrivalAirport' => 'Аэропорт прилета',
            'date' => 'Дата рейста',
            'passengers' => 'Кол-во пассажиров',
        ];
    }
}
