<?php

namespace App\Http\Requests;

use App\Rules\CheckerCountPassenger;
use App\Rules\UniquePassenger;
use Illuminate\Foundation\Http\FormRequest;

class BookingRequest extends FormRequest
{
    protected $stopOnFirstFailure = true;
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return [
            'flight_from.id' => [
                'required',
                'exists:flights,id',
            ],
            'flight_from.date' => 'required|date_format:Y-m-d',
            'flight_from' => [
                'array',
                'exclude_without:flight_from.id',
                'exclude_without:flight_from.date',
                new CheckerCountPassenger('from', count($this->input('passengers', []))),
            ],
            'flight_back.id' => [
                'sometimes',
                'different:flight_from.id',
                'exists:flights,id',
            ],
            'flight_back.date' => 'sometimes|date_format:Y-m-d|after:flight_from.date',
            'flight_back' => [
                'array',
                'exclude_without:flight_back.id',
                'exclude_without:flight_back.date',
                new CheckerCountPassenger('to', count($this->input('passengers', []))),
            ],
            'passengers' => 'required|array',
            'passengers.*.first_name' => 'required|alpha|max:30',
            'passengers.*.last_name' => 'required|alpha|max:30',
            'passengers.*.document_number' => [
                'required',
                'numeric',
                'digits:10',
            ],
            'passengers.*' => [
                new UniquePassenger(),
            ],
            'passengers.*.birth_date' => 'required|date_format:Y-m-d|before_or_equal:today',
        ];
    }

    public function attributes(): array
    {
        return [
            'flight_from.id' => 'Идетификатор вылета',
            'flight_from.date' => 'Дата вылета',
            'flight_back.id' => 'Идетификатор прилета',
            'flight_back.date' => 'Дата прилета',
            'passengers' => 'Пассажиры',
            'passengers.*.first_name' => 'Имя',
            'passengers.*.last_name' => 'Фамилия',
            'passengers.*.document_number' => 'Номер документа',
            'passengers.*.birth_date' => 'Дата рождения',
        ];
    }
}
