<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return [
            'first_name' => 'required|alpha|max:30',
            'last_name' => 'required|alpha|max:30',
            'phone' => 'required|numeric|digits:11|unique:users',
            'document_number' => 'required|numeric|digits:10|unique:users',
            'password' => 'required|min:6|max:32',
        ];
    }
}
