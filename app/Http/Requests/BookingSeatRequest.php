<?php

namespace App\Http\Requests;

use App\Models\Booking;
use App\Models\Passenger;
use App\Rules\FlightBackAvailabilityChecker;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class BookingSeatRequest extends FormRequest
{
    protected $stopOnFirstFailure = true;
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        $bookingId = $this->route('booking') ? $this->route('booking')->id : 0;

        return [
            'type' => [
                'required',
                'in:from,back',
                new FlightBackAvailabilityChecker($bookingId),
            ],
            'passenger' => [
                'required',
                Rule::exists(Passenger::class, 'id')->where('booking_id', $bookingId)
            ],
            'seat' => [
                'required',
                Rule::in([
                    'A1', 'A2', 'A3', 'A4', 'A5', 'A6', 'A7', 'A8', 'A9', 'A10', 'A11', 'A12',
                    'B1', 'B2', 'B3', 'B4', 'B5', 'B6', 'B7', 'B8', 'B9', 'B10', 'B11', 'B12',
                    'C1', 'C2', 'C3', 'C4', 'C5', 'C6', 'C7', 'C8', 'C9', 'C10', 'C11', 'C12',
                    'D1', 'D2', 'D3', 'D4', 'D5', 'D6', 'D7', 'D8', 'D9', 'D10', 'D11', 'D12',
                ]),
                Rule::unique(Passenger::class, 'place_from')->where('booking_id', $bookingId),
                Rule::unique(Passenger::class, 'place_back')->where('booking_id', $bookingId),
            ],
        ];
    }

    public function attributes(): array
    {
        return [
            'passenger' => 'Пассажир',
            'seat' => 'Место',
            'type' => 'Тип',
        ];
    }

    public function messages(): array
    {
        return [
            'passenger.exists' => 'Данный пасажер не существует или не зарегистрирован на указанный рейс',
            'seat.unique' => 'Место уже занято',
        ];
    }
}
