<?php

namespace App\Http\Resources;

use App\Models\Booking;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Knuckles\Scribe\Attributes\ResponseField;

/**
 * @property Booking $resource
 * @mixin Booking
 */
class BookingCreateResource extends JsonResource
{
    public static $wrap = false;

    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */

    #[ResponseField('code', 'string', 'Код бронирования')]
    public function toArray($request): array
    {
        return [
            'code' => $this->resource->code,
        ];
    }
}
