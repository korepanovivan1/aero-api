<?php

namespace App\Http\Resources;

use App\Models\Airport;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Knuckles\Scribe\Attributes\ResponseField;

/**
 * @property Airport $resource
 * @mixin Airport
 */
class AirportResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    #[ResponseField('name', 'string', 'Название аэропорта')]
    #[ResponseField('iata', 'string', 'Код аэропорта')]
    #[ResponseField('city', 'string', 'Название города')]
    public function toArray($request): array
    {
        return [
            'name' => $this->resource->name,
            'iata' => $this->resource->iata,
            'city' => $this->resource->city,
        ];
    }
}
