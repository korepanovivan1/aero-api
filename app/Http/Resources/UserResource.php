<?php

namespace App\Http\Resources;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Knuckles\Scribe\Attributes\ResponseField;

/**
 * @property User $resource
 * @mixin User
 */
class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */

    #[ResponseField('first_name', 'string', 'Имя')]
    #[ResponseField('last_name', 'string', 'Фамилия')]
    #[ResponseField('phone', 'string', 'Номер телефона')]
    #[ResponseField('document_number', 'string', 'Номер документа (пасторт)')]
    public function toArray($request): array
    {
        return [
            'first_name' => $this->resource->first_name,
            'last_name' => $this->resource->last_name,
            'phone' => $this->resource->phone,
            'document_number' => $this->resource->document_number,
        ];
    }
}
