<?php

namespace App\Http\Resources\Booking;

use App\Http\Resources\PassengerResource;
use App\Models\Booking;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Knuckles\Scribe\Attributes\ResponseField;

/**
 * @property Booking $resource
 * @mixin Booking
 */
class BookingResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    #[ResponseField('id', 'integer', 'Идентификатор бронирования')]
    #[ResponseField('code', 'string', 'Код бронирования')]

    #[ResponseField('flight_from', 'object', 'Информация о рейсе туда')]
    #[ResponseField('flight_from.id', 'integer', 'Идентификатор рейста')]
    #[ResponseField('flight_from.code', 'string', 'Код рейста')]
    #[ResponseField('flight_from.cost', 'integer', 'Стоимость рейста')]

    #[ResponseField('flight_from.airport_from', 'object', 'Информация об аэропорте вылета')]
    #[ResponseField('flight_from.airport_from.city', 'string', 'Город расположения аэропорта')]
    #[ResponseField('flight_from.airport_from.airport', 'string', 'Название аэропорта')]
    #[ResponseField('flight_from.airport_from.iata', 'string', 'Код аэропорта')]
    #[ResponseField('flight_from.airport_from.date', 'string', 'Дата вылета')]
    #[ResponseField('flight_from.airport_from.time', 'string', 'Время вылета')]

    #[ResponseField('flight_from.airport_back', 'object', 'Информация об аэропорте прилета')]
    #[ResponseField('flight_from.airport_back.city', 'string', 'Город расположения аэропорта')]
    #[ResponseField('flight_from.airport_back.airport', 'string', 'Название аэропорта')]
    #[ResponseField('flight_from.airport_back.iata', 'string', 'Код аэропорта')]
    #[ResponseField('flight_from.airport_back.date', 'string', 'Дата прилета')]
    #[ResponseField('flight_from.airport_back.time', 'string', 'Время прилета')]


    #[ResponseField('flight_back', 'object', 'Информация о рейсе обратно (при наличии)')]
    #[ResponseField('flight_back.id', 'integer', 'Идентификатор рейста')]
    #[ResponseField('flight_back.code', 'string', 'Код рейста')]
    #[ResponseField('flight_back.cost', 'integer', 'Стоимость рейста')]

    #[ResponseField('flight_back.airport_from', 'object', 'Информация об аэропорте вылета')]
    #[ResponseField('flight_back.airport_from.city', 'string', 'Город расположения аэропорта')]
    #[ResponseField('flight_back.airport_from.airport', 'string', 'Название аэропорта')]
    #[ResponseField('flight_back.airport_from.iata', 'string', 'Код аэропорта')]
    #[ResponseField('flight_back.airport_from.date', 'string', 'Дата вылета')]
    #[ResponseField('flight_back.airport_from.time', 'string', 'Время вылета')]

    #[ResponseField('flight_back.airport_back', 'object', 'Информация об аэропорте прилета')]
    #[ResponseField('flight_back.airport_back.city', 'string', 'Город расположения аэропорта')]
    #[ResponseField('flight_back.airport_back.airport', 'string', 'Название аэропорта')]
    #[ResponseField('flight_back.airport_back.iata', 'string', 'Код аэропорта')]
    #[ResponseField('flight_back.airport_back.date', 'string', 'Дата прилета')]
    #[ResponseField('flight_back.airport_back.time', 'string', 'Время прилета')]

    #[ResponseField('cost', 'integer', 'Итоговая сумма бронирования')]

    #[ResponseField('passengers', 'array', 'Пассажиры бронирования')]

    #[ResponseField('passengers[].id', 'integer', 'Индентификтор пассажира')]
    #[ResponseField('passengers[].first_name', 'string', 'Имя')]
    #[ResponseField('passengers[].last_name', 'string', 'Фамилия')]
    #[ResponseField('passengers[].birth_date', 'string', 'Дата рождения')]
    #[ResponseField('passengers[].document_number', 'integer', 'Номер документа')]
    #[ResponseField('passengers[].place_from', 'string', 'Место пассажира туда (может быть null)')]
    #[ResponseField('passengers[].place_back', 'string', 'Место пассажира обратно (может быть null)')]
    public function toArray($request): array
    {
        return [
            'id' => $this->resource->id,
            'code' => $this->resource->code,
            'flight_from' => FlightResource::make($this->resource->flightFrom)->additional([
                'date' => $this->resource->date_from,
            ]),
            'flight_back' => FlightResource::make($this->resource->flightBack)->additional([
                'date' => $this->resource->date_back,
            ]),
            'cost' => ($this->resource->flightFrom ? $this->resource->flightFrom->cost : 0)
                + ($this->resource->flightBack ? $this->resource->flightBack->cost : 0),
            'passengers' => PassengerResource::collection($this->resource->passengers),
        ];
    }
}
