<?php

namespace App\Http\Resources\Booking;

use App\Http\Resources\Flight\AirportResource;
use App\Models\Flight;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Knuckles\Scribe\Attributes\ResponseField;
use Knuckles\Scribe\Attributes\ResponseFromFile;

/**
 * @property Flight $resource
 * @mixin Flight
 */
class FlightResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    #[ResponseField('id', 'integer', 'Идентификатор перелета')]
    #[ResponseField('code', 'string', 'Код перелета')]
    #[ResponseField('cost', 'integer', 'Стоимость перелета')]
    public function toArray($request): array
    {
        return [
            'id' => $this->resource->id,
            'code' => $this->resource->code,
            'airport_from' => AirportResource::make($this->resource->departureAirport)->additional([
                'date' => $this->additional['date'],
                'time' => $this->resource->time_from,
            ]),
            'airport_back' => AirportResource::make($this->resource->arrivalAirport)->additional([
                'date' => $this->additional['date'],
                'time' => $this->resource->time_to,
            ]),
            'cost' => $this->resource->cost
        ];
    }
}
