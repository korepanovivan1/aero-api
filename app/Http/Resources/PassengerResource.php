<?php

namespace App\Http\Resources;

use App\Models\Passenger;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Knuckles\Scribe\Attributes\ResponseField;

/**
 * @property Passenger $resource
 * @mixin Passenger
 */
class PassengerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    #[ResponseField('id', 'integer', 'Индентификтор пассажира')]
    #[ResponseField('first_name', 'string', 'Имя')]
    #[ResponseField('last_name', 'string', 'Фамилия')]
    #[ResponseField('birth_date', 'string', 'Дата рождения')]
    #[ResponseField('document_number', 'integer', 'Номер документа')]
    #[ResponseField('place_from', 'string', 'Место пассажира туда (может быть null)')]
    #[ResponseField('place_back', 'string', 'Место пассажира обратно (может быть null)')]
    public function toArray($request): array
    {
        return [
            'id' => $this->resource->id,
            'first_name' => $this->resource->first_name,
            'last_name' => $this->resource->last_name,
            'birth_date' => $this->resource->birth_date,
            'document_number' => $this->resource->document_number,
            'place_from' => $this->resource->place_from,
            'place_back' => $this->resource->place_back,
        ];
    }
}
