<?php

namespace App\Http\Resources\Flight;

use App\Models\Flight;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Knuckles\Scribe\Attributes\ResponseField;

/**
 * @property Flight $resource
 * @mixin Flight
 */
class FlightResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    #[ResponseField('id', 'integer', 'Идентификатор рейса')]
    #[ResponseField('code', 'string', 'Код рейса')]

    #[ResponseField('from', 'object', 'Информация об аэропорте вылета')]
    #[ResponseField('from.city', 'string', 'Город расположения аэропорта')]
    #[ResponseField('from.airport', 'string', 'Название аэропорта')]
    #[ResponseField('from.iata', 'string', 'Код аэропорта')]
    #[ResponseField('from.date', 'string', 'Дата вылета')]
    #[ResponseField('from.time', 'string', 'Время вылета')]

    #[ResponseField('back', 'object', 'Информация об аэропорте прилета')]
    #[ResponseField('back.city', 'string', 'Город расположения аэропорта')]
    #[ResponseField('back.airport', 'string', 'Название аэропорта')]
    #[ResponseField('back.iata', 'string', 'Код аэропорта')]
    #[ResponseField('back.date', 'string', 'Дата прилета')]
    #[ResponseField('back.time', 'string', 'Время прилета')]
    #[ResponseField('cost', 'integer', 'Стоимость перелета')]
    public function toArray($request): array
    {
        return [
            'id' => $this->resource->id,
            'code' => $this->resource->code,
            'from' => AirportResource::make($this->resource->departureAirport)->additional([
                'date' => $this->resource->date,
                'time' => $this->resource->time_from,
            ]),
            'back' => AirportResource::make($this->resource->arrivalAirport)->additional([
                'date' => $this->resource->date,
                'time' => $this->resource->time_to,
            ]),
            'cost' => $this->resource->cost
        ];
    }
}
