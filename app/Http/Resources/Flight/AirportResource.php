<?php

namespace App\Http\Resources\Flight;

use App\Models\Airport;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Knuckles\Scribe\Attributes\ResponseField;

/**
 * @property Airport $resource
 * @mixin Airport
 */
class AirportResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    #[ResponseField('city', 'integer', 'Город')]
    #[ResponseField('airport', 'string', 'Аэропорт')]
    #[ResponseField('iata', 'integer', 'Код аэропорта')]
    #[ResponseField('date', 'integer', 'Дата вылета')]
    #[ResponseField('time', 'integer', 'Время вылета')]
    public function toArray($request): array
    {
        return array_merge([
            'city' => $this->resource->city,
            'airport' => $this->resource->name,
            'iata' => $this->resource->iata,
        ], $this->additional);
    }
}
