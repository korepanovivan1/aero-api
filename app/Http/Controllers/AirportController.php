<?php

namespace App\Http\Controllers;

use App\Http\Resources\AirportResource;
use App\Models\Airport;
use App\Services\AirportService;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Knuckles\Scribe\Attributes\Endpoint;
use Knuckles\Scribe\Attributes\QueryParam;
use Knuckles\Scribe\Attributes\ResponseFromApiResource;

class AirportController extends Controller
{
    public function __construct(
        private AirportService $airportService
    ) {}

    #[Endpoint('Список аэропортов')]
    #[QueryParam('filter[name]', 'string', 'Позволяет выполнить поиск по названию аэропорта', 'false', 'Шереметьево')]
    #[QueryParam('filter[city]', 'string', 'Позволяет выполнить поиск по названию города', 'false', 'Москва')]
    #[QueryParam('filter[iata]', 'string', 'Позволяет выполнить поиск по коду', 'false', 'VKO')]
    #[QueryParam('sort', 'string', 'Отсортировать по названию аэропорта (минус меняет направление на от Я до А)', 'false', '-name')]
    #[ResponseFromApiResource(AirportResource::class, Airport::class, 200, collection: true, paginate: 10)]
    public function get(): AnonymousResourceCollection
    {
        return AirportResource::collection($this->airportService->getAirports());
    }
}
