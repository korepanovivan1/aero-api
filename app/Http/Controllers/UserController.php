<?php

namespace App\Http\Controllers;

use App\Http\Resources\Booking\BookingResource;
use App\Http\Resources\UserResource;
use App\Models\Booking;
use App\Models\User;
use App\Services\UserService;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Facades\Auth;
use Knuckles\Scribe\Attributes\Authenticated;
use Knuckles\Scribe\Attributes\Endpoint;
use Knuckles\Scribe\Attributes\Response as ResponseScribe;
use Knuckles\Scribe\Attributes\ResponseFromApiResource;

#[Authenticated]
#[ResponseScribe(
    content: ['message' => 'Неаутентифицированный'],
    status: 401,
    description: 'Был передан несуществующий токен'
)]
class UserController extends Controller
{
    public function __construct(
        private UserService $userService
    ) {}

    #[Endpoint('Информация о пользователе')]
    #[ResponseFromApiResource(UserResource::class, User::class, 200)]
    public function get(): UserResource
    {
        return UserResource::make(Auth::user());
    }

    #[Endpoint('Бронирования пользователя')]
    #[ResponseFromApiResource(BookingResource::class, Booking::class, 200)]
    public function booking(): AnonymousResourceCollection
    {
        /** @var User $user */
        $user = Auth::user();
        return BookingResource::collection($this->userService->getBooking($user));
    }
}
