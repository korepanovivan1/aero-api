<?php

namespace App\Http\Controllers;

use App\Http\Requests\FlightSearchRequest;
use App\Http\Requests\FlightSeatsRequest;
use App\Http\Resources\Flight\FlightResource;
use App\Models\Flight;
use App\Services\FlightService;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Knuckles\Scribe\Attributes\BodyParam;
use Knuckles\Scribe\Attributes\Endpoint;
use Knuckles\Scribe\Attributes\QueryParam;
use Knuckles\Scribe\Attributes\Response;
use Knuckles\Scribe\Attributes\Response as ResponseScribe;
use Knuckles\Scribe\Attributes\ResponseFromApiResource;
use Knuckles\Scribe\Attributes\UrlParam;

#[ResponseScribe(
    content: [
        'message' => 'Текст ошибки',
        'errors' => ['название_поля' => ['ошибка', 'ошибка'], 'название_поля_2' => ['ошибка', 'ошибка']]
    ],
    status: 422,
    description: 'Ошибка валидации'
)]
class FlightController extends Controller
{
    public function __construct(
        private FlightService $flightService
    ) {
    }

    #[Endpoint('Список рейсов')]
    #[UrlParam('type', 'string', 'Направление. Обязательное. Только from или back.', true, 'from')]
    #[QueryParam('departureAirport', 'string', 'Код аэропорта вылета.', true, 'VKO')]
    #[QueryParam('arrivalAirport', 'string', 'Код аэропорта вылета.', true, 'DME')]
    #[QueryParam('date', 'string', 'Дата рейса.', true, '2023-04-10')]
    #[QueryParam('passengers', 'integer', 'Кол-во пассажиров.', true, 3)]
    #[ResponseFromApiResource(FlightResource::class, Flight::class, 200, collection: true, paginate: 10)]
    public function getFlights(FlightSearchRequest $request, string $type): AnonymousResourceCollection
    {
        if ($type === 'from') {
            return FlightResource::collection(
                $this->flightService->getFlightsByDate(
                    $request->get('departureAirport'),
                    $request->get('arrivalAirport'),
                    $request->get('date')
                )
            );
        }

        return FlightResource::collection(
            $this->flightService->getFlightsByDate(
                $request->get('arrivalAirport'),
                $request->get('departureAirport'),
                $request->get('date')
            )
        );
    }

    #[Endpoint('Карта самолета', 'Метод вернет список свободных и занятых мест на рейс.')]
    #[UrlParam('flight_id', 'integer', 'Номер рейса. Обязательное, рейс должен существовать.', true, 1)]
    #[QueryParam('date', 'string', 'Дата на которое число планируется рейс. Обязательное, позже текущей даты.', true,
        '2023-04-10')]
    #[Response([
            "A" => ['1' => false, '2' => false, '3' => false, '4' => false, '5' => false, '6' => false, '7' => false, '8' =>false, '9' => false, '10' => false, '11' => false, '12' => false],
            "B" => ['1' => false, '2' => false, '3' => false, '4' => false, '5' => false, '6' => false, '7' => false, '8' =>false, '9' => false, '10' => false, '11' => false, '12' => false],
            "C" => ['1' => false, '2' => false, '3' => false, '4' => false, '5' => false, '6' => false, '7' => false, '8' =>false, '9' => false, '10' => false, '11' => false, '12' => false],
            "D" => ['1' => false, '2' => false, '3' => false, '4' => false, '5' => false, '6' => false, '7' => false, '8' =>false, '9' => false, '10' => false, '11' => false, '12' => false],
        ], 200, 'false - место свободно, true - занято'
    )]
    public function getSeats(FlightSeatsRequest $request, Flight $flight): array
    {
        return $this->flightService->getSeats($flight->id, $request->get('date'));
    }
}
