<?php

namespace App\Http\Controllers;

use App\Http\Requests\BookingRequest;
use App\Http\Requests\BookingSeatRequest;
use App\Http\Resources\Booking\BookingResource;
use App\Http\Resources\BookingCreateResource;
use App\Http\Resources\PassengerResource;
use App\Models\Booking;
use App\Models\Passenger;
use App\Services\BookingService;
use Knuckles\Scribe\Attributes\BodyParam;
use Knuckles\Scribe\Attributes\Endpoint;
use Knuckles\Scribe\Attributes\Response as ResponseScribe;
use Knuckles\Scribe\Attributes\ResponseFromApiResource;
use Knuckles\Scribe\Attributes\UrlParam;

class BookingController extends Controller
{
    public function __construct(
        private BookingService $bookingService
    ) {}

    #[Endpoint('Информация о бронировании')]
    #[UrlParam('booking_code', 'string', 'Код бронирования. Обязательное, должен существовать.', true, 'UDHTZ')]
    #[ResponseScribe(
        content: ['message' => 'Не найдено'],
        status: 404,
        description: 'Бронирование не найдено'
    )]
    #[ResponseFromApiResource(BookingResource::class, Booking::class, 200)]
    public function get(Booking $booking): BookingResource
    {
        return BookingResource::make($booking);
    }

    #[Endpoint('Создать бронирование')]
    #[ResponseScribe(
        content: [
            'message' => 'Текст ошибки',
            'errors' => ['название_поля' => ['ошибка', 'ошибка'], 'название_поля_2' => ['ошибка', 'ошибка']]
        ],
        status: 422,
        description: 'Ошибка валидации'
    )]
    #[ResponseFromApiResource(BookingCreateResource::class, Booking::class, 200)]
    #[BodyParam('flight_from', 'object', 'Информация о полете туда')]
    #[BodyParam('flight_from.id', 'integer', 'ID рейса. Обязательно должен существовать.', true, 1)]
    #[BodyParam('flight_from.date', 'string', 'Дата рейса', true, '2023-04-10')]
    #[BodyParam('flight_back', 'object', 'Информация о полете обратно')]
    #[BodyParam('flight_back.id', 'integer', 'ID рейса. Если заполнен, то должен существовать.', true, 2)]
    #[BodyParam('flight_back.date', 'string', 'Дата рейса', true, '2023-04-12')]
    #[BodyParam('passengers', 'object', 'Пассажиры рейса')]
    #[BodyParam('passengers[].first_name', 'string', 'Имя (Только буквы, максимум 30 символов)', true, 'Иван')]
    #[BodyParam('passengers[].last_name', 'string', 'Фамилия (Только буквы, максимум 30 символов)', true, 'Иванов')]
    #[BodyParam('passengers[].document_number', 'integer', 'Серия и номер паспорта (Только цифры, максимум 10 символов, уникальный)', true, '9414123123')]
    #[BodyParam('passengers[].birth_date', 'string', 'Дата рождения. Не позже текущей даты.', true, '1999-04-12')]
    public function booking(BookingRequest $request): BookingCreateResource
    {
        return BookingCreateResource::make($this->bookingService->booking($request->validated()));
    }

    #[Endpoint('Забронировать место')]
    #[ResponseScribe(
        content: [
            'message' => 'Текст ошибки',
            'errors' => ['название_поля' => ['ошибка', 'ошибка'], 'название_поля_2' => ['ошибка', 'ошибка']]
        ],
        status: 422,
        description: 'Ошибка валидации'
    )]
    #[ResponseScribe(
        content: ['message' => 'Не найдено'],
        status: 404,
        description: 'Бронирование не найдено'
    )]
    #[UrlParam('booking_code', 'string', 'Код бронирования. Обязательное, должен существовать.', true, 'UDHTZ')]
    #[ResponseFromApiResource(PassengerResource::class, Passenger::class, 200)]
    #[BodyParam('type', 'string', 'Направление. Может быть from или back', true, 'from')]
    #[BodyParam('passenger', 'integer', 'ID пассажира', true, 1)]
    #[BodyParam('seat', 'string', 'Место. От A1 до A12. От B1 до B12. От C1 до C12. От D1 до D12.', true, 'D1')]
    public function bookingSeat(BookingSeatRequest $bookingSeatRequest, Booking $booking): PassengerResource
    {
        return PassengerResource::make($this->bookingService->bookingSeat($booking, $bookingSeatRequest->validated()));
    }
}
