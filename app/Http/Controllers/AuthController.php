<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\Http\Resources\InfoResource;
use App\Http\Resources\LoginSuccessResource;
use App\Services\AuthService;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Knuckles\Scribe\Attributes\Authenticated;
use Knuckles\Scribe\Attributes\BodyParam;
use Knuckles\Scribe\Attributes\Endpoint;
use Knuckles\Scribe\Attributes\Response as ResponseScribe;
use Symfony\Component\HttpFoundation\Response;

class AuthController extends Controller
{
    private AuthService $authService;

    public function __construct(AuthService $authService)
    {
        $this->authService = $authService;
    }

    #[Endpoint('Регистрация', 'Регистрация – неавторизованный пользователь должен иметь возможность зарегистрироваться в системе на странице с регистрацией.')]
    #[ResponseScribe(
        content: ['message' => 'Пользователь успешно загеристирован'],
        status: 201,
        description: 'Пользователь зарегистрирован'
    )]
    #[ResponseScribe(
        content: [
            'message' => 'Текст ошибки',
            'errors' => ['название_поля' => ['ошибка', 'ошибка'], 'название_поля_2' => ['ошибка', 'ошибка']]
        ],
        status: 422,
        description: 'Ошибка валидации'
    )]
    #[BodyParam('first_name', 'string', 'Имя (Только буквы, максимум 30 символов)', true, 'Иван')]
    #[BodyParam('last_name', 'string', 'Фамилия (Только буквы, максимум 30 символов)', true, 'Иванов')]
    #[BodyParam('phone', 'integer', 'Телефон (Только цифры, максимум 11 символов, уникальный)', true, '89224512155')]
    #[BodyParam('document_number', 'integer', 'Серия и номер паспорта (Только цифры, максимум 10 символов, уникальный)', true, '9414123123')]
    #[BodyParam('password', 'string', 'Пароль (Строка, максимум 32 символа, минимум 6 символов)', true, 'P@ssW0Rd!')]
    public function register(RegisterRequest $request): JsonResponse
    {
        if ($this->authService->register($request->validated())) {
            return InfoResource::make('Пользователь успешно загеристирован')
                ->response()->setStatusCode(Response::HTTP_CREATED);
        }

        return InfoResource::make('Что-то пошло не так')
            ->response()->setStatusCode(Response::HTTP_NO_CONTENT);
    }

    #[Endpoint('Авторизация')]
    #[ResponseScribe(
        content: ['token' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vd3NyLWFwaS50ZXN0L2FwaS9hdXRoL2xvZ2luIiwiaWF0IjoxNjgwMDM1NzI3LCJuYmYiOjE2ODAwMzU3MjcsImp0aSI6ImVHM3czNW1OaE9YdlRISmMiLCJzdWIiOiIxMyIsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.GnY3GRnn-WXJgSev-4jzZB1GiLDhprNIn3bVHB2H_hM'],
        status: 200,
        description: 'Возвращается токен'
    )]
    #[ResponseScribe(
        content: ['message' => 'Неверный логин или пароль'],
        status: 401,
        description: 'Введен неверно логин или пароль'
    )]
    #[ResponseScribe(
        content: [
            'message' => 'Текст ошибки',
            'errors' => ['название_поля' => ['ошибка', 'ошибка'], 'название_поля_2' => ['ошибка', 'ошибка']]
        ],
        status: 422,
        description: 'Ошибка валидации'
    )]
    #[BodyParam('phone', 'integer', 'Телефон (Только цифры, максимум 11 символов, уникальный)', true, '89224512155')]
    #[BodyParam('password', 'string', 'Пароль (Строка, максимум 32 символа, минимум 6 символов)', true, 'P@ssW0Rd!')]
    public function login(LoginRequest $request): LoginSuccessResource|JsonResponse
    {
        if ($token = Auth::attempt($request->validated())) {
            return LoginSuccessResource::make($token);
        }

        return InfoResource::make('Неверный логин или пароль')
            ->response()->setStatusCode(Response::HTTP_UNAUTHORIZED);
    }

    #[Authenticated]
    #[Endpoint('Обновление токена', 'На данный момент токен бессрочный, но на всякий случай существует метод его перевыпуска. Он удлаяет старый и выдает новый. Входной параметр - токен в заголовке.')]
    #[ResponseScribe(
        content: ['token' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vd3NyLWFwaS50ZXN0L2FwaS9hdXRoL2xvZ2luIiwiaWF0IjoxNjgwMDM1NzI3LCJuYmYiOjE2ODAwMzU3MjcsImp0aSI6ImVHM3czNW1OaE9YdlRISmMiLCJzdWIiOiIxMyIsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.GnY3GRnn-WXJgSev-4jzZB1GiLDhprNIn3bVHB2H_hM'],
        status: 200,
        description: 'Возвращается токен'
    )]
    #[ResponseScribe(
        content: ['message' => 'Неаутентифицированный'],
        status: 401,
        description: 'Был передан несуществующий токен'
    )]
    public function refresh(): LoginSuccessResource|JsonResponse
    {
        if ($token = Auth::refresh()) {
            return LoginSuccessResource::make($token);
        }

        return InfoResource::make('Что-то пошло не так')
            ->response()->setStatusCode(Response::HTTP_NO_CONTENT);
    }

    #[Authenticated]
    #[Endpoint('Удаление токена', 'Чтобы разлогинить пользователя недостаточно просто удалить ключ из локального хранилища. Нужно забыть его на сервре.')]
    #[ResponseScribe(
        content: ['message' => 'Вы успешно вышли из системы'],
        status: 200,
        description: 'Вы успешно вышли из системы'
    )]
    #[ResponseScribe(
        content: ['message' => 'Неаутентифицированный'],
        status: 401,
        description: 'Был передан несуществующий токен'
    )]
    public function logout(): InfoResource
    {
        Auth::logout();
        return InfoResource::make('Вы успешно вышли из системы');
    }
}
