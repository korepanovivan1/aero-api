<?php

namespace App\Providers;

use App\Interfaces\AirportRepositoryInterface;
use App\Interfaces\BookingRepositoryInterface;
use App\Interfaces\PassengerRepositoryInterface;
use App\Interfaces\UserRepositoryInterface;
use App\Repositories\AirportRepository;
use App\Repositories\BookingRepository;
use App\Repositories\PassengerRepository;
use App\Repositories\UserRepository;
use Illuminate\Support\ServiceProvider;
use App\Interfaces\FlightRepositoryInterface;
use App\Repositories\FlightRepository;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->app->bind(UserRepositoryInterface::class, UserRepository::class);
        $this->app->bind(AirportRepositoryInterface::class, AirportRepository::class);
        $this->app->bind(FlightRepositoryInterface::class, FlightRepository::class);
        $this->app->bind(BookingRepositoryInterface::class, BookingRepository::class);
        $this->app->bind(PassengerRepositoryInterface::class, PassengerRepository::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(): void
    {
        //
    }
}
