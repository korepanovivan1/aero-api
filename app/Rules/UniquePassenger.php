<?php

namespace App\Rules;

use App\Interfaces\PassengerRepositoryInterface;
use Illuminate\Contracts\Validation\Rule;

class UniquePassenger implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value): bool
    {
        return !app(PassengerRepositoryInterface::class)
            ->uniquePassenger(
                $value['document_number'],
                request()->input('flight_from.id'),
                request()->input('flight_back.id')
            );
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message(): string
    {
        return 'Вы уже зарегистрированны на данный рейс';
    }
}
