<?php

namespace App\Rules;

use App\Interfaces\BookingRepositoryInterface;
use Illuminate\Contracts\Validation\Rule;

class FlightBackAvailabilityChecker implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(private int $bookingId)
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value): bool
    {
        if ($value === 'back') {
            return app(BookingRepositoryInterface::class)->checkFlightBackAvailability($this->bookingId);
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message(): string
    {
        return 'В данном бронировании не предусмотрен обратный рейс';
    }
}
