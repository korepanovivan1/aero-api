<?php

namespace App\Rules;

use App\Interfaces\BookingRepositoryInterface;
use App\Models\Booking;
use Illuminate\Contracts\Validation\Rule;

class CheckerCountPassenger implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(
        private string $type,
        private int $currentCountPassenger,
    ) {
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value): bool
    {
        if (!\DateTime::createFromFormat('Y-m-d', $value['date'])) {
            return true;
        }

        return app(BookingRepositoryInterface::class)
                ->countPassengerToBooking(
                    $this->type,
                    $value['date'],
                    $value['id']
                ) + $this->currentCountPassenger <= Booking::MAX_COUNT_PASSENGER;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message(): string
    {
        return 'Нет мест на данный рейс';
    }
}
