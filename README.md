REST API авиакомпании
========================================

Запуск проекта
-------------
Конфигурация окружения находится в docker-compose.
Для запуска проекта выполнить:
> cd docker
>
> docker-compose up -d

Скопировать **.env.example** и назвать его **.env**

После запуска docker установить пакеты composer, накатить миграции и сиды, сгенерировать документацию по api.


Зайти в контейнер с php-fpm и выполнить:
> composer install
> 
> php artisan key:generate
> 
> php artisan migrate --seed
> 
> php artisan scribe:generate

Для отображения проекта в браузере добавить в hosts файл строку
> 127.0.0.1 aero-api.test

Конфиг подключения к БД находится в **.env**.

Документация по api доступна по адресу `${url}/docs` (по умолчанию aero-api.test/docs).

