document.addEventListener("DOMContentLoaded", function() {
    // Flatpickr
    flatpickr(".flatpickr-minimum");
    flatpickr(".flatpickr-datetime", {
        enableTime: true,
        dateFormat: "Y-m-d H:i",
    });
    flatpickr(".flatpickr-human", {
        altInput: true,
        altFormat: "F j, Y",
        dateFormat: "Y-m-d",
    });
    flatpickr(".flatpickr-multiple", {
        mode: "multiple",
        dateFormat: "Y-m-d"
    });
    flatpickr(".flatpickr-range", {
        mode: "range",
        dateFormat: "Y-m-d"
    });
    flatpickr(".flatpickr-time", {
        enableTime: true,
        noCalendar: true,
        dateFormat: "H:i",
    });
});
