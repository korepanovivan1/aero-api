<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ {
    AuthController,
    AirportController,
    UserController,
    BookingController,
    FlightController,
};

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('airport', [AirportController::class, 'get']);
Route::get('flights/{type}', [FlightController::class, 'getFlights'])
    ->where('type', 'from|back');

Route::post('booking', [BookingController::class, 'booking']);
Route::get('booking/{booking:code}', [BookingController::class, 'get']);

Route::get('flight/{flight}/seats', [FlightController::class, 'getSeats']);

Route::patch('booking/{booking:code}/seat', [BookingController::class, 'bookingSeat']);

Route::group(['prefix' => 'auth'], function () {
    Route::post('register', [AuthController::class, 'register']);
    Route::post('login', [AuthController::class, 'login']);
    Route::group(['middleware' => 'auth:api'], function () {
        Route::post('refresh', [AuthController::class, 'refresh']);
        Route::post('logout', [AuthController::class, 'logout']);
    });
});

Route::group(['middleware' => 'auth:api'], function () {
    Route::group(['prefix' => 'user'], function () {
        Route::get('', [UserController::class, 'get']);
        Route::get('booking', [UserController::class, 'booking']);
    });
});
