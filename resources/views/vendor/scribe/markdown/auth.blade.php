# Авторизация

@if(!$isAuthed)
This API is not authenticated.
@else
{!! $extraAuthInfo !!}
@endif
